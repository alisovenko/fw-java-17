package com.fitzwilliam.lec16.tasks;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * @author alisovenko
 *         12/7/16.
 */
public class QueryBooks {
    static final String DATABASE_URL = "jdbc:mysql://localhost/books";
    private Connection conn;
    private PreparedStatement sql;

    public QueryBooks() throws SQLException {
        // TODO implement
    }

    /**
     * Queries database table books to fetch all books which editionNumber is equal to {@code editionNumber} and copyright is equal to
     * {@code copyright}.
     *
     * It returns the list of books ISBNs
     */
    public List<String> queryBooksByEditionNumberAndCopyright(int editionNumber, int copyRight) throws SQLException {
        // TODO implement!
        return null;
    }

    public void closeResources() {
        // TODO implement
    }
}
