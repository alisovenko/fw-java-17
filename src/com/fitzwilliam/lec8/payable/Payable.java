package com.fitzwilliam.lec8.payable;
// Payable interface declaration.

public interface Payable {
    double getPaymentAmount(); // calculate payment; no implementation
}
