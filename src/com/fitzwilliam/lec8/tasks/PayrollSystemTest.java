package com.fitzwilliam.lec8.tasks;

import java.time.LocalDate;

public class PayrollSystemTest {
    public static void main(String args[]) {
        // create subclass objects
        SalariedEmployee salariedEmployee =
                new SalariedEmployee("John", "Smith", "111-11-1111", 800.00, LocalDate.parse("1988-10-05"));
        HourlyEmployee hourlyEmployee =
                new HourlyEmployee("Karen", "Price", "222-22-2222", 16.75, 40, LocalDate.parse("1988-05-05"));
        CommissionEmployee commissionEmployee =
                new CommissionEmployee(
                        "Sue", "Jones", "333-33-3333", 10000, .06, LocalDate.parse("1988-10-05"));
        BasePlusCommissionEmployee basePlusCommissionEmployee =
                new BasePlusCommissionEmployee(
                        "Bob", "Lewis", "444-44-4444", 5000, .04, 300, LocalDate.parse("1988-10-05"));

        System.out.println("Employees processed individually:\n");

        System.out.printf("%s\n%s: $%,.2f\n\n",
                salariedEmployee, "earned", salariedEmployee.weaklyEarnings());
        System.out.printf("%s\n%s: $%,.2f\n\n",
                hourlyEmployee, "earned", hourlyEmployee.weaklyEarnings());
        System.out.printf("%s\n%s: $%,.2f\n\n",
                commissionEmployee, "earned", commissionEmployee.weaklyEarnings());
        System.out.printf("%s\n%s: $%,.2f\n\n",
                basePlusCommissionEmployee,
                "earned", basePlusCommissionEmployee.weaklyEarnings());

        // create four-element Employee array
        Employee employees[] = new Employee[4];

        // initialize array with Employees
        employees[0] = salariedEmployee;
        employees[1] = hourlyEmployee;
        employees[2] = commissionEmployee;
        employees[3] = basePlusCommissionEmployee;

        System.out.println("==========================================");
        System.out.println("Employees processed polymorphically:\n");

        // generically process each element in array employees
        for (Employee currentEmployee : employees) {
            System.out.println(currentEmployee); // invokes toString

            if (LocalDate.now().getMonthValue() == currentEmployee.getBirthday().getMonthValue()) {
                System.out.printf(
                        "earned $%,.2f\n\n", currentEmployee.weaklyEarnings() * 4 + 100000);
            } else {
                System.out.printf(
                        "earned $%,.2f\n\n", currentEmployee.weaklyEarnings() * 4);
            }
        } // end for

        // get type name of each object in employees array
        for (int j = 0; j < employees.length; j++)
            System.out.printf("Employee %d is a %s\n", j,
                    employees[j].getClass().getName());
    }
}
