package com.fitzwilliam.lec8.tasks;
// Employee abstract superclass.

import java.time.LocalDate;

public abstract class Employee {
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;
    private final LocalDate birthday;

    // three-argument constructor
    public Employee(String first, String last, String ssn, LocalDate birthday) {
        firstName = first;
        lastName = last;
        socialSecurityNumber = ssn;
        this.birthday = birthday;
    } // end three-argument Employee constructor

    // return first name
    public String getFirstName() {
        return firstName;
    }

    // return last name
    public String getLastName() {
        return lastName;
    }

    // return social security number
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    // return String representation of Employee object
    public String toString() {
        return String.format("%s %s\nsocial security number: %s",
                getFirstName(), getLastName(), getSocialSecurityNumber());
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    // abstract method overridden by subclasses (no implementation!)
    public abstract double weaklyEarnings();
}

