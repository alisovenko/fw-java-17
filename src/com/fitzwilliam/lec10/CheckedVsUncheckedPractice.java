package com.fitzwilliam.lec10;

import java.io.IOException;

/**
 * @author alisovenko
 *         5/10/17.
 */
public class CheckedVsUncheckedPractice {
    public static void main(String[] args) {
        try {
            doRaiseCheckedException();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String s = args[0];
        if (s == null)
            System.out.println("Please provide the first argument");
        else
            doRaiseUncheckedException(s);
    }

    private static void doRaiseUncheckedException(String s) {
        if (s == null)
            throw new IllegalArgumentException("s must be not null!");
        System.out.println("foo");
    }

    private static void doRaiseCheckedException() throws IOException {
        throw new IOException();
    }
}
