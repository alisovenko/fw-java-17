package com.fitzwilliam.lec5.tasks;

import java.util.Scanner;

/**
 * @author alisovenko
 *         4/19/17.
 */
public class Plane {
    public static void main(String[] args) {
        boolean[] seats = new boolean[12];
        int firstClassCounter = 0;
        int economyCounter = 6;
        Scanner scanner = new Scanner(System.in);

        while (firstClassCounter < 6 || economyCounter < 12) {
            System.out.println();
            System.out.println("Please type 1 for First Class and Please type 2 for Economy");
            int c = scanner.nextInt();

            switch (c) {
                case 1:
                    firstClassCounter = registerFirstClass(seats, firstClassCounter);
                    break;
                case 2:
                    economyCounter = registerEconomyClass(seats, economyCounter);
                    break;
                default:
                    System.err.println("Please enter correct number!");
            }
        }
        System.out.println("No seats left! Next flight leaves in 3 hours");
    }

    private static int registerFirstClass(boolean[] seats, int firstClassCounter) {
        if (firstClassCounter >= 6) {
            System.out.println("Unfortunately first class is all booked, you can try economy seats!");
            return firstClassCounter;
        }

        seats[firstClassCounter++] = true;
        System.out.printf("Congratulations, you've just booked seat number %d in first class!\n", firstClassCounter);
        return firstClassCounter;
    }

    private static int registerEconomyClass(boolean[] seats, int economyCounter) {
        if (economyCounter >= 12) {
            System.out.println("Unfortunately economy seats are all booked, you can try first class!");
            return economyCounter;
        }

        seats[economyCounter++] = true;
        System.out.printf("Congratulations, you've just booked economy seat number %d!\n", economyCounter);
        return economyCounter;
    }
}
