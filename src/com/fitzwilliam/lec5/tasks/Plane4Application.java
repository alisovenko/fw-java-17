package com.fitzwilliam.lec5.tasks;

import java.util.Scanner;

/**
 * @author alisovenko
 *         4/20/17.
 */
public class Plane4Application {
    public static void main(String[] args) {
        Plane4 planeSystem = new Plane4();
        Scanner scanner = new Scanner(System.in);

        int userInput;
        do {
            System.out.println("\nWelcome to plane registration system! Print 1 to book first class seat, 2 - for economy seats, 3 to print seats map and -1 to terminate the application");
            userInput = scanner.nextInt();

            switch (userInput) {
                case 1:
                case 2:
                    if (!planeSystem.bookSeat(userInput))
                        userInput = -1;
                    break;
                case 3:
                    planeSystem.printSeats();
                    break;
                case -1:
                    break;
                default:
                    System.out.println("Sorry I don't understand...");
            }
        } while (userInput != -1);

        System.out.println("Bye!");
    }

}
