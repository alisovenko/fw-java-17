package com.fitzwilliam.lec5.tasks;

import java.util.Scanner;

/**
 * This version removes some redundancy that was in previous version and uses the general method for registering the seat
 *
 * @author alisovenko
 *         4/19/17.
 */
public class Plane2 {
    public static void main(String[] args) {
        boolean[] seats = new boolean[12];
        int firstClassCounter = 0;
        int economyCounter = 6;
        Scanner scanner = new Scanner(System.in);

        while (firstClassCounter < 6 || economyCounter < 12) {
            System.out.println();
            System.out.println("Please type 1 for First Class and Please type 2 for Economy");
            int c = scanner.nextInt();

            switch (c) {
                case 1:
                    firstClassCounter = registerSeat(seats, firstClassCounter, "first class", 6);
                    break;
                case 2:
                    economyCounter = registerSeat(seats, economyCounter, "economy", 12);
                    break;
                default:
                    System.err.println("Please enter correct number!");
            }
        }
        System.out.println("\nNo seats left! Next flight leaves in 3 hours");
    }

    private static int registerSeat(boolean[] seats, int counter, String className, int threashold) {
        if (counter >= threashold) {
            System.out.printf("Unfortunately all seats for class %s are booked, you can try other class!\n", className);
            return counter;
        }

        seats[counter++] = true;
        System.out.printf("Congratulations, you've just booked seat number %d in class %s!\n", counter, className);
        return counter;
    }
}
