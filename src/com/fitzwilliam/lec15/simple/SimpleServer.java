package com.fitzwilliam.lec15.simple;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDateTime;

/**
 * @author alisovenko
 *         11/28/16.
 */
public class SimpleServer {
    public static void main(String[] args) throws IOException {
        try (ServerSocket listener = new ServerSocket(5000)) {
            try (Socket socket = listener.accept()) {
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                out.print(LocalDateTime.now());
                out.flush();
            }
        }
    }
}
