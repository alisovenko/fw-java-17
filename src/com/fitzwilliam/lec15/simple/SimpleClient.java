package com.fitzwilliam.lec15.simple;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * @author alisovenko
 *         11/28/16.
 */
public class SimpleClient {
    public static void main(String[] args) throws IOException {
        Socket s = new Socket("localhost", 5000);
        try {
            BufferedReader input = new BufferedReader(new InputStreamReader(s.getInputStream()));
            String response = input.readLine();
            JOptionPane.showMessageDialog(null, response);
        } finally {
            s.close();
        }
    }
}
