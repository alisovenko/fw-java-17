package com.fitzwilliam.lec2.fig1;

/**
 * This is cool class to start from.
 *
 * @author alisovenko
 *         4/3/17.
 */
public class GradeBook {
    private String courseName;

    public GradeBook(String name) {
        courseName = name;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseName() {
        return courseName;
    }

    public void displayMessage() {
        System.out.println("Course name: " + courseName);
    }

}
