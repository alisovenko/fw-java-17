package com.fitzwilliam.lec3.teacher;

/**
 * @author alisovenko
 *         4/5/17.
 */
public class CastsPractice {
    public static void main(String[] args) {
        short s = 1;
        short p = 3;
        short f = (short)(s + p);

        int i = Integer.MAX_VALUE;
        long bPositive = Long.MAX_VALUE; // 11111...11;
        long bNegative = Long.MIN_VALUE; // 00000.0000;

        System.out.printf("Long positive: %d\n", bPositive);
        System.out.printf("Long negative: %d\n", bNegative);
        System.out.printf("Long positive cast to int: %d\n", (int)bPositive);
        System.out.printf("Long negative cast to int: %d\n", (int)bNegative);

    }
}
