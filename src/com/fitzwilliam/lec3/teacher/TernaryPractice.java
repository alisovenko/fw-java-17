package com.fitzwilliam.lec3.teacher;

/**
 * @author alisovenko
 *         4/5/17.
 */
public class TernaryPractice {
    private int n;

    public static void main(String[] args) {
        int n = (1 != 1) ? 4 : 10;

        int m;
        if (1 == 1)
            m = 4;
        else
            m = 10;

        System.out.println(n);
    }
}
